# OpenML dataset: bank8FM

https://www.openml.org/d/572

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**:   
**Source**: Unknown - Date unknown  
**Please cite**:   

A family of datasets synthetically generated from a simulation of how bank-customers choose their banks. Tasks are
based on predicting the fraction of bank customers who leave the bank because of full queues. The bank family of
datasets are generated from a simplistic simulator, which simulates the queues in a series of banks. The simulator was
constructed with the explicit purpose of generating a family of datasets for DELVE. Customers come from several
residential areas, choose their preferred bank depending on distances and have tasks of varying complexity, and various
levels of patience. Each bank has several queues, that open and close according to demand. The tellers have various
effectivities, and customers may change queue, if their patience expires. In the rej prototasks, the object is to predict the
rate of rejections, ie the fraction of customers that are turned away from the bank because all the open tellers have full
queues.
Source: collection of regression datasets by Luis Torgo (ltorgo@ncc.up.pt) at
http://www.ncc.up.pt/~ltorgo/Regression/DataSets.html
Orginal source: DELVE repository of data.
Characteristics: Data set contains 8192 (4500+3692) cases. and 9 continuous
attributes

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/572) of an [OpenML dataset](https://www.openml.org/d/572). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/572/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/572/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/572/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

